import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';

import styles from './Button.module.scss';

const Button = ({ onClick, label, className, children, ...props }) => {
  return (
    <button
      className={classNames(styles.button, className)}
      onClick={onClick}
      {...props}
    >
      {label || children}
    </button>
  );
};

Button.propTypes = {
  label: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
};

export default Button;
