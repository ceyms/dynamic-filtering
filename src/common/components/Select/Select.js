import React from 'react';
import PropTypes from 'prop-types';
import ReactSelect from 'react-select';
import styles from './Select.module.scss';

const Select = ({ onChange, options, value, label, placeholder, isMulti }) => {
  return (
    <div className={styles.select}>
      { label && (
        <label>{label}</label>
      )}
      <ReactSelect
        value={value}
        isMulti={isMulti}
        options={options}
        onChange={onChange}
        placeholder={placeholder || `Select ${label}`}
      />
    </div>
  );
};

Select.propTypes = {
  isMulti: PropTypes.bool,
  label: PropTypes.string,
  options: PropTypes.array,
  onChange: PropTypes.func,
  placeholder: PropTypes.string,
  value: PropTypes.oneOfType([PropTypes.string, PropTypes.object, PropTypes.array]),
};

export default Select;
