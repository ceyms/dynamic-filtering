import React, { useEffect, useRef } from 'react';
import PropTypes from 'prop-types';
import Button from '../Button';

import styles from './Dropdown.module.scss';

const Dropdown = ({ isOpen, children, onClick }) => {
  const dropdownNode = useRef();

  const handleClickOutside = (e) => {
    if (dropdownNode.current && dropdownNode.current.contains(e.target)) {
      return;
    }
    onClick(false);
  };

  useEffect(() => {
    if (isOpen) {
      document.addEventListener("mousedown", handleClickOutside);
    } else {
      document.removeEventListener("mousedown", handleClickOutside);
    }

    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [isOpen]);

  return (
    <div ref={dropdownNode}>
      <Button
        label="More Filters"
        onClick={() => onClick(!isOpen)}
      />
      {isOpen && (
        <div className={styles.dropdown}>
          {children}
        </div>
      )}
    </div>
  );
};

Dropdown.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

export default Dropdown;
