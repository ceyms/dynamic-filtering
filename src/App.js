import React from 'react';
import { Route, Redirect, Switch } from 'react-router-dom';
import Dashboard from './containers/Dashboard';

const App = () => (
  <Switch>
    <Route path="/dashboard" component={Dashboard} />
    <Redirect from="/" to="/dashboard" />
  </Switch>
);

export default App;
