import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import pick from 'lodash/pick';
import DatePicker from 'react-datepicker';
import classNames from 'classnames';
import Dropdown from '../../../common/components/Dropdown';
import Button from '../../../common/components/Button';
import Select from '../../../common/components/Select';
import { colors } from '../constants';
import { getSelectedValues } from '../utils';
import styles from './Filters.module.scss';

const MoreFilters = ({ filters, onChange }) => {
  const defaultMoreFilters = pick(filters, ['startDate', 'endDate', 'color']);

  const [isDropdownOpen, setDropdownStatus] = useState(false);
  const [moreFilters, setMoreFilters] = useState(defaultMoreFilters);

  const cancelMoreFilters = () => {
    setMoreFilters(filters);
    setDropdownStatus(false);
  };

  const applyMoreFilters = () => {
    let dates = {};
    const isValidDate = moment(moreFilters.endDate).isSameOrAfter(moreFilters.startDate);

    if(moreFilters.startDate) {
      dates.startDate = moment(moreFilters.startDate).format('YYYY-MM-DD HH:mm:ss');
    }

    if(moreFilters.endDate) {
      if(!isValidDate) return alert('End date should be same or after Start date')
      dates.endDate = moment(moreFilters.endDate).format('YYYY-MM-DD HH:mm:ss');
    }

    onChange({ ...moreFilters, ...dates });
    setDropdownStatus(false);
  };

  return (
    <Dropdown
      isOpen={isDropdownOpen}
      onClick={(val) => {
        cancelMoreFilters();
        setDropdownStatus(val);
      }}
    >
      <div className={styles.dropdownField}>
        <Select
          options={colors}
          label="Color"
          isMulti
          onChange={(selected) => setMoreFilters({ ...moreFilters, color: selected.map(color => color.value) })}
          value={getSelectedValues(colors, moreFilters.color)}
        />
      </div>
      <div className={classNames(styles.dropdownField, styles.textField)}>
        <label>Start Date</label>
        <DatePicker
          selected={moreFilters.startDate ? new Date(moreFilters.startDate) : null}
          placeholderText="Click to select a start date"
          onChange={(startDate) => setMoreFilters({ ...moreFilters, startDate })}
          showTimeSelect
          timeFormat="HH:mm"
          timeIntervals={10}
          dateFormat="MMMM d, yyyy h:mm aa"
          timeCaption="time"
        />
      </div>
      <div className={classNames(styles.dropdownField, styles.textField)}>
        <label>End Date</label>
        <DatePicker
          selected={moreFilters.endDate ? new Date(moreFilters.endDate) : null}
          placeholderText="Click to select a end date"
          onChange={(endDate) => setMoreFilters({ ...moreFilters, endDate })}
          showTimeSelect
          timeFormat="HH:mm"
          timeIntervals={10}
          dateFormat="MMMM d, yyyy h:mm aa"
          timeCaption="time"
          minDate={moreFilters.startDate ? new Date(moreFilters.startDate) : null}
        />
      </div>
      <div className={styles.buttons}>
        <Button label="Cancel" className={styles.cancel} onClick={cancelMoreFilters} />
        <Button label="Apply" className={styles.apply} onClick={applyMoreFilters} />
      </div>
    </Dropdown>
  );
};

MoreFilters.propTypes = {
  filters: PropTypes.object,
  onChange: PropTypes.func.isRequired,
};

export default MoreFilters;
