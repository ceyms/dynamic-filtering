import React from 'react';
import PropTypes from 'prop-types';
import Select from '../../../common/components/Select';
import MoreFilters from './MoreFilters';
import { types, statuses } from '../constants';
import { getSelectedValues } from '../utils';

import styles from './Filters.module.scss';

const Filters = ({ filters = {}, onChange }) => (
  <div className={styles.filtersWrapper}>
    <div className={styles.filters}>
      <div className={styles.filter}>
        <div className={styles.textField}>
          <label>Search</label>
          <input
            value={filters.q || ''}
            placeholder="Search by name"
            onChange={(e) => onChange({ q: e.target.value })}
          />
        </div>
      </div>
      <div className={styles.filter}>
        <Select
          options={statuses}
          label="Status"
          isMulti
          onChange={(selected) => onChange({ status: selected.map(status => status.value) })}
          value={getSelectedValues(statuses, filters.status)}
        />
      </div>
      <div className={styles.filter}>
        <Select
          options={types}
          label="Type"
          isMulti
          onChange={(selected) => onChange({ type: selected.map(type => type.value) })}
          value={getSelectedValues(types, filters.type)}
        />
      </div>
      <div className={styles.filter}>
        <MoreFilters
          filters={filters}
          onChange={onChange}
        />
      </div>
    </div>
  </div>
);

Filters.propTypes = {
  filters: PropTypes.object,
  onChange: PropTypes.func.isRequired,
};

export default Filters;
