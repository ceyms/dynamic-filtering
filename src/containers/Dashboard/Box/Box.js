import React from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { prettyStatuses, prettyTypes, prettyColors } from '../constants';
import styles from './Box.module.scss';

const getTime = date => moment(date).format('HH:mm');
const getDate = date => moment(date).format('YYYY-MM-DD');

const Box = ({ item }) => (
  <div className={styles.box}>
    <header>
      <div className={styles.top}>
        <span>{prettyStatuses[item.status]}</span>
        <span>{prettyTypes[item.type]}</span>
      </div>
      <h3>{item.name}</h3>
      <strong>{getDate(item.startDate)} - {getDate(item.endDate)}</strong>
      <strong>{getTime(item.startDate)} - {getTime(item.endDate)}</strong>
      <span className={styles.color} style={{ background: item.color }}>
        {prettyColors[item.color]}
      </span>
    </header>
    <main>
      <div className={styles.description}>
        <h4>Description</h4>
        <p>{item.description}</p>
      </div>
    </main>
  </div>
);

Box.propTypes = {
  item: PropTypes.object,
};

export default Box;
