import data from '../../data.json';
import moment from 'moment';

const isDate = date => moment(date || null).isValid();

const getDefaultQueries = (queries) => ({
  ...queries,
  ...(!Array.isArray(queries.status) && queries.status ? { status: [queries.status] } : {}),
  ...(!Array.isArray(queries.type) && queries.type ? { type: [queries.type] } : {}),
  ...(!Array.isArray(queries.color) && queries.color ? { color: [queries.color] } : {}),
});

const getSelectedValues = (records, selectedValues) => {
  if(Array.isArray(selectedValues) && selectedValues.length) {
    return records.filter(record => selectedValues.includes(record.value));
  }
  return null;
}

const getFilteredData = (filters) => {
  return data.filter(record => {
    let isFiltered = true;

    Object.keys(filters).forEach((filter) => {
      const isArrayHasLength = Array.isArray(filters[filter]) && filters[filter].length;
      if (isArrayHasLength && !filters[filter].includes(record[filter])) {
        isFiltered = false;
      }
    });

    if(isDate(filters.startDate) && moment(filters.startDate).isAfter(moment(record.startDate))) {
      isFiltered = false;
    }
    if(isDate(filters.endDate) && moment(filters.endDate).isBefore(record.endDate)) {
      isFiltered = false;
    }

    return isFiltered;
  }).filter(record => filters.q ? record.name.toLowerCase().includes(filters.q.toLowerCase()) : true)
};

export {
  getFilteredData,
  getDefaultQueries,
  getSelectedValues,
};
