import React, { Fragment, useState } from 'react';
import PropTypes from 'prop-types';
import queryString from 'query-string';
import Box from './Box';
import Filters from './Filters';
import { getFilteredData, getDefaultQueries } from './utils';
import styles from './Dashboard.module.scss';

const parseFormat = { arrayFormat: 'comma' };

const Dashboard = ({ location, history }) => {
  const defaultQueries = getDefaultQueries(queryString.parse(location.search, parseFormat));
  const [queries, setQuery] = useState(defaultQueries);
  const filteredData = getFilteredData(queries);

  const filtersOnChange = (newQueries) => {
    const mergedQueries = { ...queries, ...newQueries };
    setQuery(mergedQueries);
    const newUrl = `/dashboard?${queryString.stringify(mergedQueries, parseFormat)}`;
    history.push(newUrl);
  };

  return (
    <Fragment>
      <Filters filters={queries} onChange={filtersOnChange} />
      <div className={styles.boxes}>
        {filteredData.map(record => (
          <div className={styles.boxWrapper} key={record.id}>
            <Box item={record} />
          </div>
        ))}
      </div>
      {Object.keys(queries).length > 0 && !filteredData.length && (
        <div className={styles.noData}>
          <strong>There is no data with these following filters</strong>
          {Object.keys(queries).map(query => (
            <div key={query}>
              {query} - {Array.isArray(queries[query]) ? queries[query].join(', ') : queries[query]}
            </div>
          ))}
        </div>
      )}
    </Fragment>
  );
};

Dashboard.propTypes = {
  location: PropTypes.object,
  history: PropTypes.object,
};

export default Dashboard;
